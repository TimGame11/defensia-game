﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class MoveSphere : MonoBehaviour
{
    public Slider mainSlider;

    public AudioSource audios, shieldsound;
    
    public GameObject textFinish;
    public Text score;
    private static List<ScoreList> dataScore;
    [SerializeField]
    private bool isWin;
    public static int limits;
    public static int lastScore = 0, lastScoreEnemy = 0;
    public int playTime = 0;
    public Text timeText, scoreText, lastText;
    public static float odometer;
    private Scene scene;
    private Rigidbody rg;
    private Vector3 input, oldPos;
    public float speed;
    private float maxSpeed;
    public float moveSpeed;
    public int jumpCount;
    public AudioClip shieldclip, multiclip;
    
    void Awake()
    {
        audios = GetComponent<AudioSource>();
        audios.Play();
    }
    void Start()
    {
        PlayerPrefs.SetInt(scene.name, 100);
        isWin = false;
        scene = SceneManager.GetActiveScene();
        oldPos = transform.position;
        jumpCount = 1;
        moveSpeed = 20f;
        speed = 15f;
        maxSpeed = 45f;
        rg = GetComponent<Rigidbody>();
    }
    void Update()
    {
        mainSlider.value = limits;
        playTime = (int)Time.timeSinceLevelLoad;
        lastScore = (int)((1000-odometer)/playTime)+lastScoreEnemy;
        lastText.text = "Score = " + lastScore;
        timeText.text = "Time = " + playTime;
        if(Input.GetKeyDown("space") && jumpCount == 1 && isWin == false)
        {
            Jump();
        }
        if (isWin == false)
        {
            Time.timeScale = 1f;
            textFinish.SetActive(false);
            Move();
        }
        if (isWin == true)
        {
            audios.Stop();
            Time.timeScale = 0f;
            textFinish.SetActive(true);
            dataScore = new List<ScoreList>();
            score.text = "Anda mendapatkan skor " + lastScore +
            " di " + scene.name + ", Top Skor di " + scene.name + " adalah " + PlayerPrefs.GetInt(scene.name);
            dataScore.Add(new ScoreList("Unknown", lastScore, "Level1"));
            if (Input.GetKeyUp(KeyCode.Space))
            {
                lastScoreEnemy = 0;
                odometer = 0;
                Time.timeScale = 1f;
                SceneManager.LoadScene(scene.name);
            }
        }
       
    }
    public void Move()
    {
        input = new Vector3 (Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        if (rg.velocity.magnitude < maxSpeed)
        {
            audios.pitch = rg.velocity.magnitude / 4f;
            audios.volume = 0.75f;
            odometer += Vector3.Distance(input, oldPos);
            limits = (int)(1000-odometer);
            scoreText.text = "Limit = " + limits;
            if((1000-odometer)<1)
            {
                lastScoreEnemy = 0;
                SceneManager.LoadScene(scene.name);
                odometer = 0;
            }
            rg.AddForce(input * moveSpeed);
        }
        if(transform.position.y < -1)
        {
            lastScoreEnemy = 0;
            SceneManager.LoadScene(scene.name);
            odometer = 0;
        } 

    }
    public void OnCollisionEnter(Collision coll){
        if (coll.gameObject.name == "Background")
        {
            jumpCount = 1;
        }
        if (coll.gameObject.name == "Finish")
        {
            isWin = true;
            if(lastScore > PlayerPrefs.GetInt(scene.name))
                PlayerPrefs.SetInt(scene.name, lastScore);
        }
    }
    public void Jump()
    {
        Vector3 atas = new Vector3(0,20,0);
        rg.AddForce(atas*speed);
        jumpCount -= 1;
    }

    public void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.name == "ShieldItem(Clone)")
        {
            shieldsound.clip = shieldclip;
            shieldsound.Play();
        }
        if(coll.gameObject.name == "MultiShoot(Clone)")
        {
            shieldsound.clip = multiclip;
            shieldsound.Play();
        }
    }

    


}
