﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DeadMoney : MonoBehaviour
{
    private Scene scene;
    public float Timer = 1f;
    public Transform projectile;
    void Start()
    {
        scene = SceneManager.GetActiveScene();
    }
    void Update()
    {
        Timer -= 1f * Time.deltaTime;
        if (Timer <= 0)
        {
            Destroy(projectile.gameObject);
        }
    }
    public void OnCollisionEnter(Collision coll){
        if (coll.gameObject.name == "Sphere")
        {
            MoveSphere.odometer += 250f;
        }
        if (coll.gameObject.name == "Background")
        {
            Destroy(projectile.gameObject);
        }
        if (coll.gameObject.name == "ShieldPivot")
        {
            Destroy(projectile.gameObject);
        }
    }
}
