﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameplay : MonoBehaviour
{
    public GameObject levelButton, playButton;
    public Text high1, high2, high3;
    private void Start()
    {
        levelButton.SetActive(false);
        high1.text = "Highscore = " + PlayerPrefs.GetInt("Level 1");
        high2.text = "Highscore = " + PlayerPrefs.GetInt("Level 2");
        high3.text = "Highscore = " + PlayerPrefs.GetInt("Level 3");
    }
    public void playgame()
    {
        playButton.SetActive(false);
        levelButton.SetActive(true);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    } 
    public void playlevel(int SceneIndex)
    {
        SceneManager.LoadScene(SceneIndex);
    }

    public void quitGame()
    {
        Application.Quit();
        Debug.Log("QUIT");
    }
}
