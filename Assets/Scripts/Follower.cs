﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    public AudioSource audios;
    public GameObject shieldObj;
    public static float itemTimer1, itemTimer2;
    public static bool isMultiShootActive, isShieldActive;
    public float rotateSpeed = 5.0f;
    public Rigidbody cylinder;
    public Quaternion targetRotation;
    public float shotForce = 1f;
    public Transform target, me, shooter;
    void Start()
    {
        audios = GetComponent<AudioSource>();
        itemTimer1 = 3f;
        itemTimer2 = 5f;
        isMultiShootActive = false;
        if (isMultiShootActive == true)
        {
            itemTimer2 = 5f;
        }
        isShieldActive = false;
        if (isShieldActive == true)
        {
            itemTimer1 = 3f;
        }
    }

    void Update()
    {
    	Plane playerPlane = new Plane(Vector3.up, transform.position);
    	Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
    	float hitdist = 0.0f;
    	if (playerPlane.Raycast (ray, out hitdist)) 
		{
        	Vector3 targetPoint = ray.GetPoint(hitdist);
        	targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
        	transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
		}
        me.position = new Vector3 (target.position.x, target.position.y, target.position.z);
        if(isMultiShootActive == false)
        {
            if(Input.GetButtonDown("Fire1") && isShieldActive == false)
            {
                PlayerShoot();
            }
        }
        if (isMultiShootActive == true)
        {
            itemTimer2 -= 1 * Time.deltaTime;
            if (itemTimer2 <= 0)
            {
                isMultiShootActive = false;
                itemTimer2 = 5f;
            }
            if(Input.GetButtonDown("Fire1"))
            {
                MultiShoot();
            }
        }
        if (isShieldActive == false)
        {
            shieldObj.SetActive(false);
        }
        if (isShieldActive == true)
        {
            itemTimer1 -= 1 * Time.deltaTime;
            shieldObj.SetActive(true);
            if (itemTimer1 <= 0)
            {
                isShieldActive = false;
                itemTimer1 = 3f;
            }
        }
    }
    public void PlayerShoot()
    {
        audios.Play();
        Rigidbody shot = Instantiate(cylinder, shooter.position, targetRotation * Quaternion.Euler(0f,0f,0f)) as Rigidbody;
        shot.GetComponent<Rigidbody>().AddForce(shot.transform.forward * shotForce);
    }
    public void MultiShoot()
    {
        audios.Play();
        Rigidbody shot = Instantiate(cylinder, shooter.position + new Vector3(0f,0f,0.1f), targetRotation * Quaternion.Euler(0f,0f,0f)) as Rigidbody;
        shot.GetComponent<Rigidbody>().AddForce(shot.transform.forward * shotForce);
        Rigidbody shot2 = Instantiate(cylinder, shooter.position + new Vector3(0.1f,0f,0.1f), targetRotation * Quaternion.Euler(0f,5f,0f)) as Rigidbody;
        shot2.GetComponent<Rigidbody>().AddForce(shot2.transform.forward * shotForce);
        Rigidbody shot3 = Instantiate(cylinder, shooter.position + new Vector3(-0.1f,0f,0.1f), targetRotation * Quaternion.Euler(0f,-5f,0f)) as Rigidbody;
        shot3.GetComponent<Rigidbody>().AddForce(shot3.transform.forward * shotForce);
        Rigidbody shot7 = Instantiate(cylinder, shooter.position + new Vector3(0.1f,0f,0f), targetRotation * Quaternion.Euler(0f,10f,0f)) as Rigidbody;
        shot7.GetComponent<Rigidbody>().AddForce(shot7.transform.forward * shotForce);
        Rigidbody shot8 = Instantiate(cylinder, shooter.position + new Vector3(-0.1f,0f,0f), targetRotation * Quaternion.Euler(0f,-10f,0f)) as Rigidbody;
        shot8.GetComponent<Rigidbody>().AddForce(shot8.transform.forward * shotForce);      
    }
}
