﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public Rigidbody projectile;
    public Transform shotPos;
    public static float shotForce = 1000f;
    public float Timer = 1f;
    private int rotateDegree;
    public GameObject enemy;
    [SerializeField]
    private int lives;
    void Start()
    {
        lives = 5;
    }
    void Update()
    {
        rotateDegree = Random.Range(45,90);
        Shoot();
    }
    public void Shoot()
    {
        Timer -= 1f * Time.deltaTime;
        if (Timer <= 0)
        {
        shotPos.Rotate(0,rotateDegree,0);
        Rigidbody shot = Instantiate(projectile, shotPos.position + new Vector3(0f,0f,1f), Quaternion.Euler(0f,0f,90f)) as Rigidbody;
        shot.AddForce(shotPos.forward * shotForce);
        Timer = 0.1f;
        }
    }
    public void OnCollisionEnter (Collision coll)
    {
        if (coll.gameObject.name == "Capsule(Clone)" && lives >= 0)
        {
            lives = lives - 1;
        }
        if (coll.gameObject.name == "Capsule(Clone)" && lives < 0)
        {
            MoveSphere.lastScoreEnemy += 20;
            Destroy(enemy);
        }
        if (coll.gameObject.name == "ShieldPivot")
        {
            MoveSphere.lastScoreEnemy += 20;
            Destroy(enemy);
        }
    }
}
