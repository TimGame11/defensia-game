﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFire : MonoBehaviour
{
    private int lives;
    public GameObject enemy;
    public Rigidbody projectile;
    public Transform shotPos, target;
    public static float shotForce = 1100f;
    public static float Timer = 0.5f, rotateAroundSpeed = 50f, rotationSpeed = 50f;
    public Transform[] patrolPoints;
    private int currentPoint;
    [SerializeField]
    private float moveSpeed = 3f;
    public float minDistance, distance;
    void Start()
    {
        lives = 5;
        transform.position = patrolPoints[0].position;
        currentPoint = 0;
        minDistance = 7f;
    }
    void Update()
    {
        if(transform.position == patrolPoints[currentPoint].position)
        {
            currentPoint = Random.Range (0, patrolPoints.Length);
        }
        transform.position = Vector3.MoveTowards(transform.position, patrolPoints[currentPoint].position, moveSpeed * Time.deltaTime);
        distance = Vector3.Distance(shotPos.transform.position, target.transform.position);
        if(distance < minDistance)
        {
            Shoot();
        }
    }
    public void Shoot()
    {
        Timer -= 1 * Time.deltaTime;
        if (Timer <= 0)
        {
        var curRot = shotPos.rotation;
        shotPos.RotateAround(shotPos.position, Vector3.up, rotateAroundSpeed * Time.deltaTime);
        shotPos.rotation = Quaternion.Slerp(curRot, Quaternion.LookRotation(
            target.position - shotPos.position), rotationSpeed*Time.deltaTime);
        Rigidbody shot = Instantiate(projectile, shotPos.position + new Vector3(-0.5f,0f,-0.5f), shotPos.rotation) as Rigidbody;
        shot.AddForce(shotPos.forward * shotForce);
        Timer = 0.5f;
        }
    }
    public void OnCollisionEnter (Collision coll)
    {
        if (coll.gameObject.name == "Capsule(Clone)" && lives >= 0)
        {
            lives = lives - 1;
        }
        if (coll.gameObject.name == "Capsule(Clone)" && lives < 0)
        {
            MoveSphere.lastScoreEnemy += 20;
            Destroy(enemy);
        }
        if (coll.gameObject.name == "ShieldPivot")
        {
            MoveSphere.lastScoreEnemy += 20;
            Destroy(enemy);
        }
    }
}