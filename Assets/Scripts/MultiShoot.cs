﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiShoot : MonoBehaviour
{
    public GameObject itemm;
    public float Timer = 3f;
    void Update()
    {
        Timer -= 1f * Time.deltaTime;
        if (Timer <= 0)
        {
            Destroy(itemm.gameObject);
        }
    }
    public void OnTriggerEnter(Collider coll){
        if (coll.gameObject.name == "Sphere")
        {
            Follower.isMultiShootActive = true;
            Follower.isShieldActive = false;
            Follower.itemTimer2 = 5f;
            Debug.Log("Dapat item multi-shoot");
            Destroy(itemm.gameObject);
        }
    }
}
