﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class buttonScript : MonoBehaviour
{
    public GameObject activeButton, inactiveButton, yuhu;
    void Start()
    {
        yuhu.SetActive(false);
    }
    public void playgame()
    {
        inactiveButton.SetActive(false);
        activeButton.SetActive(true);
    }
}
