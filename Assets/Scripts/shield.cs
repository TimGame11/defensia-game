﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shield : MonoBehaviour
{
    public float rotateSpeed = 5.0f;
    public Quaternion targetRotation;
    public Transform target, me;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    	Plane playerPlane = new Plane(Vector3.up, transform.position);
    	Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
    	float hitdist = 0.0f;
    	if (playerPlane.Raycast (ray, out hitdist)) 
		{
        	Vector3 targetPoint = ray.GetPoint(hitdist);
        	targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
        	transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
		}
        me.position = new Vector3 (target.position.x, target.position.y, target.position.z);
        /*if(Input.GetButtonDown("Fire1"))
        {
            me.rotation = Quaternion.Slerp(transform.rotation, targetRotation * Quaternion.Euler(0f,180f,0f), rotateSpeed * Time.deltaTime * 5.0f);
        } */  
    }
}
