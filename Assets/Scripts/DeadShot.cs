﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadShot : MonoBehaviour
{
    public float Timer = 1f;
    public Transform projectile;
    void Update()
    {
        Timer -= 1 * Time.deltaTime;
        if (Timer <= 0)
        {
            Destroy(projectile.gameObject);
        }
    }
    public void OnTriggerEnter(Collider coll){
    }
    public void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.name == "Background" || coll.gameObject.name == "ShootingEnemy")
        {
            Destroy(projectile.gameObject);
        }
    }
}
