﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    public Text score;
    private static List<ScoreList> dataScore;
    //static List<ScoreList> dataScore = new List<ScoreList>();
    void Start()
    {
        dataScore = new List<ScoreList>();
        score.text = Play.user_id + " mendapatkan skor " + MoveSphere.lastScore +
        " di level 1, Top Skor di Level 1 adalah " + PlayerPrefs.GetInt("temp");
        dataScore.Add(new ScoreList(Play.user_id, MoveSphere.lastScore, WinBox.level));
        foreach(ScoreList a in dataScore)
        {
            Debug.Log(a.nameString + " " + a.level + " " + a.scoreInt);
        }
        MoveSphere.lastScore = 0;
        MoveSphere.lastScoreEnemy = 0;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
            ReloadScene("Level1");
    }
    public void ReloadScene(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }
}
