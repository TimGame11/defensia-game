﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnItem : MonoBehaviour
{
    public GameObject spawnItem;
    public Transform[] spawnPoints;
    private Transform currentPoint;
    private int index;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Launch", 1f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        index = Random.Range (0, spawnPoints.Length);
        currentPoint = spawnPoints[index];
    }
    void Launch()
        {
            GameObject item = Instantiate(spawnItem, currentPoint.position, currentPoint.rotation) as GameObject;
        }
}